<div class="featured-ideas">
    <div class="row">
        <div class="col-md">
            <h1 id="featuredIdeaH1">Ideas</h1>
        </div>
    </div>
    <div class="ideasRow">
        <div class="row" id="ideasRow">
            <div class="col-md">
                <ul>
                    <li><img src="../img/ecommimages/Ideas_01.jpg" alt="Featured Idea 1"></li>
                    <li><h2 class="ideasH2">Ideas for your home improvement</h2></li>
                    <li><p class="ideasP" style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu iaculis odio. Quisque et efficitur eros. Cras id rhoncus tortor.</p></li>
                    <li><span class="ideasSpan" style="float: left"><a href="#">Learn more ></a></span></li>
                </ul>
            </div>
            <div class="col-md">
                <ul>
                    <li><img src="../img/ecommimages/Ideas_02.jpg" alt="Featured Idea 2"></li>
                    <li><h2 class="ideasH2">Ideas for your home improvement</h2></li>
                    <li><p class="ideasP" style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu iaculis odio. Quisque et efficitur eros. Cras id rhoncus tortor.</p></li>
                    <li><span class="ideasSpan" style="float: left"><a href="#">Learn more ></a></span></li>
                </ul>
            </div>
            <div class="col-md">
                <ul>
                    <li><img src="../img/ecommimages/ideas_03.jpg" alt="Featured Idea 3"></li>
                    <li><h2 class="ideasH2">Ideas for your home improvement</h2></li>
                    <li><p class="ideasP" style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu iaculis odio. Quisque et efficitur eros. Cras id rhoncus tortor.</p></li>
                    <li><span class="ideasSpan" style="float: left"><a href="#">Learn more ></a></span></li>
                </ul>
            </div>
        </div>
    </div>
</div>