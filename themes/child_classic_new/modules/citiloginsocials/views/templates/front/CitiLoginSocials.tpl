
<div class="social-buttons">
    <p class="text-lg-center text-md-center text-xs-center">
        <a href="{$link->getModuleLink('citiloginsocials','authenticate',["action"=>"google","mode" => "request","page_type" => {$page.page_name}])|escape:'html':'UTF-8'}" class="btn btn-secondary" id="googleBtn">
			<span>
				<i class="fa fa-google-plus-square" aria-hidden="true" style="font-size: 1.2rem !important;"></i>
				&nbsp;{l s='Continue with Google'}
			</span>
        </a>
    </p>
    <p class="text-lg-center text-md-center text-xs-center">
        <a href="{$link->getModuleLink('citiloginsocials','authenticate',["action"=>"facebook","mode" => "request","page_type" => {$page.page_name}])|escape:'html':'UTF-8'}" class="btn btn-secondary" id="facebookBtn">
			<span>
				<i class="fa fa-facebook-official" aria-hidden="true" style="font-size: 1.2rem !important;"></i>
				&nbsp;{l s='Continue with Facebook'}
			</span>
        </a>
    </p>
</div>