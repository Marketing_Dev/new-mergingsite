<div class="featured-product">
    <div class="row">
        <div class="col-md">
            <h1 id="featuredProductH1">Featured products</h1>
        </div>
    </div>
    <div class="productRow" align="center">
        <div class="row" align="center">
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_01.jpg" alt="Featured Products 1">
                    <div id="desc">
                        <p id="description">
                            Boston Bay
                        </p>
                        <p id="description">
                            Portable Aircon 1.0HP
                        </p>
                        <p id="description">
                            BB12356
                        </p>
                        <p id="price">
{*                        <span id="price">*}
                            ₱ 12,345.67
{*                        </span>*}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_02.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                        {*                        <span id="price">*}
                        ₱ 12,345.67
                        {*                        </span>*}
                    </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_03.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                        {*                        <span id="price">*}
                        ₱ 12,345.67
                        {*                        </span>*}
                    </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_04.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                        {*                        <span id="price">*}
                        ₱ 12,345.67
                        {*                        </span>*}
                    </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_05.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                        {*                        <span id="price">*}
                        ₱ 12,345.67
                        {*                        </span>*}
                    </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_06.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                        {*                        <span id="price">*}
                        ₱ 12,345.67
                        {*                        </span>*}
                    </p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>