<div id="block_social_links">
            <ul>
                <li><a href="https://www.facebook.com/CitiHardwarePH/" target="_blank" class="socialLinksFooter"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/citihardware/" target="_blank" class="socialLinksFooter"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="https://www.pinterest.ph/digital1557" target="_blank" class="socialLinksFooter"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
                <li><a href="https://www.youtube.com/c/CitiHardwarePH" target="_blank" class="socialLinksFooter"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
            </ul>
</div></a>