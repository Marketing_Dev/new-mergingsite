<div class="featured-category">
    <div class="row">
        <div class="col-md">
            <h1 id="featuredH1">Featured categories</h1>
        </div>
    </div>
    <div class="categoryRow">
        <div class="row" id="categoryRow">
            <div class="col-md-4">
                <div class="contain">
                    <img src="../img/ecommimages/appliances.jpg" alt="Featured Category 1">
                </div>
            </div>
            <div class="col-md-4">
                <div class="contain">
                    <img src="../img/ecommimages/hardware.jpg" alt="Featured Category 2">
                </div>
            </div>
            <div class="col-md-4">
                <div class="contain">
                    <img src="../img/ecommimages/electrical.jpg" alt="Featured Category 3">
                </div>
            </div>
        </div>
    </div>
</div>


