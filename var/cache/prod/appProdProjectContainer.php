<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerJt2qwfh\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerJt2qwfh/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerJt2qwfh.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerJt2qwfh\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerJt2qwfh\appProdProjectContainer([
    'container.build_hash' => 'Jt2qwfh',
    'container.build_id' => '150cc340',
    'container.build_time' => 1615857355,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerJt2qwfh');
