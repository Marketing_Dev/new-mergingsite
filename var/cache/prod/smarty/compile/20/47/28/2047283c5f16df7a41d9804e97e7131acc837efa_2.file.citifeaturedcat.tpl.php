<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-26 07:14:38
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citifeaturedcat/views/templates/hooks/citifeaturedcat.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_605d195e4bef25_84732753',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2047283c5f16df7a41d9804e97e7131acc837efa' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citifeaturedcat/views/templates/hooks/citifeaturedcat.tpl',
      1 => 1616657377,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605d195e4bef25_84732753 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="featured-category">
    <div class="row">
        <div class="col-md">
            <h1 id="featuredH1">Featured categories</h1>
        </div>
    </div>
    <div class="categoryRow">
        <div class="row" id="categoryRow">
            <div class="col-md-4">
                <div class="contain">
                    <img src="../img/ecommimages/appliances.jpg" alt="Featured Category 1">
                </div>
            </div>
            <div class="col-md-4">
                <div class="contain">
                    <img src="../img/ecommimages/hardware.jpg" alt="Featured Category 2">
                </div>
            </div>
            <div class="col-md-4">
                <div class="contain">
                    <img src="../img/ecommimages/electrical.jpg" alt="Featured Category 3">
                </div>
            </div>
        </div>
    </div>
</div>


<?php }
}
