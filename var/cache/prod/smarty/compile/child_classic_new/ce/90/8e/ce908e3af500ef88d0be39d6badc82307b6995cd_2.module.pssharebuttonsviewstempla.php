<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-24 14:30:52
  from 'module:pssharebuttonsviewstempla' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_605adc9cc7ef06_66160683',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ce908e3af500ef88d0be39d6badc82307b6995cd' => 
    array (
      0 => 'module:pssharebuttonsviewstempla',
      1 => 1614926922,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605adc9cc7ef06_66160683 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_466303186605adc9cc7dfa2_40907613', 'social_sharing');
?>

<?php }
/* {block 'social_sharing'} */
class Block_466303186605adc9cc7dfa2_40907613 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'social_sharing' => 
  array (
    0 => 'Block_466303186605adc9cc7dfa2_40907613',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php if ($_smarty_tpl->tpl_vars['social_share_links']->value) {?>
    <div class="social-links">
      <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Share','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</span>
      <ul>
        <li><a href="https://www.facebook.com/CitiHardwarePH/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true" style="font-size: 2.5rem !important;"></i></a></li>
        <li><a href="https://www.instagram.com/citihardware/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2.5rem !important;"></i></a></li>
        <li><a href="https://www.pinterest.ph/digital1557" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true" style="font-size: 2.5rem !important;"></i></a></li>
      </ul>
    </div>
  <?php }
}
}
/* {/block 'social_sharing'} */
}
