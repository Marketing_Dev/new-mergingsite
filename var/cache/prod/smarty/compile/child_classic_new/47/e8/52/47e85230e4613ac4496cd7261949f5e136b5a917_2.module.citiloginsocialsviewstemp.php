<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-23 11:15:38
  from 'module:citiloginsocialsviewstemp' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_60595d5a6b3285_53027514',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '47e85230e4613ac4496cd7261949f5e136b5a917' => 
    array (
      0 => 'module:citiloginsocialsviewstemp',
      1 => 1614153179,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60595d5a6b3285_53027514 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="social-buttons">
    <p class="text-lg-center text-md-center text-xs-center">
        <a href="<?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');
$_prefixVariable22 = ob_get_clean();
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('citiloginsocials','authenticate',array("action"=>"google","mode"=>"request","page_type"=>$_prefixVariable22)),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="btn btn-secondary" id="googleBtn">
			<span>
				<i class="fa fa-google-plus-square" aria-hidden="true" style="font-size: 1.2rem !important;"></i>
				&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Continue with Google'),$_smarty_tpl ) );?>

			</span>
        </a>
    </p>
    <p class="text-lg-center text-md-center text-xs-center">
        <a href="<?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');
$_prefixVariable23 = ob_get_clean();
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('citiloginsocials','authenticate',array("action"=>"facebook","mode"=>"request","page_type"=>$_prefixVariable23)),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="btn btn-secondary" id="facebookBtn">
			<span>
				<i class="fa fa-facebook-official" aria-hidden="true" style="font-size: 1.2rem !important;"></i>
				&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Continue with Facebook'),$_smarty_tpl ) );?>

			</span>
        </a>
    </p>
</div><?php }
}
