<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-26 07:15:57
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_605d19ad3bd6d4_16167389',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '34fdfe8d990763c3b3310e1948ebc0f02637678c' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/page.tpl',
      1 => 1616374733,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605d19ad3bd6d4_16167389 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1978505841605d19ad3b9984_31571984', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_header_container'} */
class Block_825760873605d19ad3b9f51_55365383 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_1906999053605d19ad3baf69_73533886 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_1651812873605d19ad3bb763_78196778 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_515676392605d19ad3ba9e6_17334233 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1906999053605d19ad3baf69_73533886', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1651812873605d19ad3bb763_78196778', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_1950325732605d19ad3bc8d4_75358703 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_1555081238605d19ad3bc372_02318563 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1950325732605d19ad3bc8d4_75358703', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_1978505841605d19ad3b9984_31571984 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_1978505841605d19ad3b9984_31571984',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_825760873605d19ad3b9f51_55365383',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_515676392605d19ad3ba9e6_17334233',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_1906999053605d19ad3baf69_73533886',
  ),
  'page_content' => 
  array (
    0 => 'Block_1651812873605d19ad3bb763_78196778',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_1555081238605d19ad3bc372_02318563',
  ),
  'page_footer' => 
  array (
    0 => 'Block_1950325732605d19ad3bc8d4_75358703',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main"> 

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_825760873605d19ad3b9f51_55365383', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_515676392605d19ad3ba9e6_17334233', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1555081238605d19ad3bc372_02318563', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
