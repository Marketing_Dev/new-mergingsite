<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-26 07:15:38
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_605d199a609a13_98639749',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '68a8dcb06c170322794cda821c6d17385a25a53d' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/index.tpl',
      1 => 1615769752,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605d199a609a13_98639749 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2116922269605d199a607ea3_35581192', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_217841929605d199a6083c4_28856924 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_659748434605d199a608c64_23244973 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_777175845605d199a608982_68847304 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_659748434605d199a608c64_23244973', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_2116922269605d199a607ea3_35581192 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_2116922269605d199a607ea3_35581192',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_217841929605d199a6083c4_28856924',
  ),
  'page_content' => 
  array (
    0 => 'Block_777175845605d199a608982_68847304',
  ),
  'hook_home' => 
  array (
    0 => 'Block_659748434605d199a608c64_23244973',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_217841929605d199a6083c4_28856924', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_777175845605d199a608982_68847304', 'page_content', $this->tplIndex);
?>


      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
