<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-31 09:04:39
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citifeaturedprod/views/templates/hooks/citifeaturedprod.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_6063caa77c1108_42917050',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7389a3a8601a06ec4e204ab414d3826f06c0a8ef' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citifeaturedprod/views/templates/hooks/citifeaturedprod.tpl',
      1 => 1617004031,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063caa77c1108_42917050 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="featured-product">
    <div class="row">
        <div class="col-md">
            <h1 id="featuredProductH1">Featured products</h1>
        </div>
    </div>
    <div class="productRow" align="center">
        <div class="row" align="center">
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_01.jpg" alt="Featured Products 1">
                    <div id="desc">
                        <p id="description">
                            Boston Bay
                        </p>
                        <p id="description">
                            Portable Aircon 1.0HP
                        </p>
                        <p id="description">
                            BB12356
                        </p>
                        <p id="price">
                            ₱ 12,345.67
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_02.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                                                ₱ 12,345.67
                                            </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_03.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                                                ₱ 12,345.67
                                            </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_04.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                                                ₱ 12,345.67
                                            </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_05.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                                                ₱ 12,345.67
                                            </p>
                </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="prodContain">
                <img src="../img/ecommimages/Featured Products_06.jpg" alt="Featured Products 1">
                <div id="desc">
                    <p id="description">
                        Boston Bay
                    </p>
                    <p id="description">
                        Portable Aircon 1.0HP
                    </p>
                    <p id="description">
                        BB12356
                    </p>
                    <p id="price">
                                                ₱ 12,345.67
                                            </p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
