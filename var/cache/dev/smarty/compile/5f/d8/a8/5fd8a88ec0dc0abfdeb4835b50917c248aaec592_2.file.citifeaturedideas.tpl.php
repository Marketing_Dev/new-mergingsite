<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-31 09:04:39
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citifeaturedideas/views/templates/hooks/citifeaturedideas.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_6063caa77c25a9_13993848',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5fd8a88ec0dc0abfdeb4835b50917c248aaec592' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citifeaturedideas/views/templates/hooks/citifeaturedideas.tpl',
      1 => 1616114597,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063caa77c25a9_13993848 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="featured-ideas">
    <div class="row">
        <div class="col-md">
            <h1 id="featuredIdeaH1">Ideas</h1>
        </div>
    </div>
    <div class="ideasRow">
        <div class="row" id="ideasRow">
            <div class="col-md">
                <ul>
                    <li><img src="../img/ecommimages/Ideas_01.jpg" alt="Featured Idea 1"></li>
                    <li><h2 class="ideasH2">Ideas for your home improvement</h2></li>
                    <li><p class="ideasP" style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu iaculis odio. Quisque et efficitur eros. Cras id rhoncus tortor.</p></li>
                    <li><span class="ideasSpan" style="float: left"><a href="#">Learn more ></a></span></li>
                </ul>
            </div>
            <div class="col-md">
                <ul>
                    <li><img src="../img/ecommimages/Ideas_02.jpg" alt="Featured Idea 2"></li>
                    <li><h2 class="ideasH2">Ideas for your home improvement</h2></li>
                    <li><p class="ideasP" style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu iaculis odio. Quisque et efficitur eros. Cras id rhoncus tortor.</p></li>
                    <li><span class="ideasSpan" style="float: left"><a href="#">Learn more ></a></span></li>
                </ul>
            </div>
            <div class="col-md">
                <ul>
                    <li><img src="../img/ecommimages/ideas_03.jpg" alt="Featured Idea 3"></li>
                    <li><h2 class="ideasH2">Ideas for your home improvement</h2></li>
                    <li><p class="ideasP" style="text-align: justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu iaculis odio. Quisque et efficitur eros. Cras id rhoncus tortor.</p></li>
                    <li><span class="ideasSpan" style="float: left"><a href="#">Learn more ></a></span></li>
                </ul>
            </div>
        </div>
    </div>
</div><?php }
}
