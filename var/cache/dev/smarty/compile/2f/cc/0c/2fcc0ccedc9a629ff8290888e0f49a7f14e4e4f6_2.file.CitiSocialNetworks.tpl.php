<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-31 09:04:40
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citisocialnetworks/views/templates/hooks/CitiSocialNetworks.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_6063caa8777671_63408045',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2fcc0ccedc9a629ff8290888e0f49a7f14e4e4f6' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/modules/citisocialnetworks/views/templates/hooks/CitiSocialNetworks.tpl',
      1 => 1617151992,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063caa8777671_63408045 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="block_social_links">
            <ul>
                <li><a href="https://www.facebook.com/CitiHardwarePH/" target="_blank" class="socialLinksFooter"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/citihardware/" target="_blank" class="socialLinksFooter"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="https://www.pinterest.ph/digital1557" target="_blank" class="socialLinksFooter"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
                <li><a href="https://www.youtube.com/c/CitiHardwarePH" target="_blank" class="socialLinksFooter"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
            </ul>
</div></a><?php }
}
