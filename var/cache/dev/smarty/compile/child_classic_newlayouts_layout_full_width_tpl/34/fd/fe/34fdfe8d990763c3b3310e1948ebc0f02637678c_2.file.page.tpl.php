<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-31 09:04:40
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_6063caa86b8f93_37682522',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '34fdfe8d990763c3b3310e1948ebc0f02637678c' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/page.tpl',
      1 => 1616374733,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063caa86b8f93_37682522 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7423560196063caa86b6b99_71648430', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_header_container'} */
class Block_2984652576063caa86b6f57_88993821 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_3656795776063caa86b7a94_23450090 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_3979912836063caa86b7f19_47793146 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_15260574056063caa86b77b0_59707010 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3656795776063caa86b7a94_23450090', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3979912836063caa86b7f19_47793146', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_6428178876063caa86b87b4_29024854 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_9510180596063caa86b8513_25077239 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6428178876063caa86b87b4_29024854', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_7423560196063caa86b6b99_71648430 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_7423560196063caa86b6b99_71648430',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_2984652576063caa86b6f57_88993821',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_15260574056063caa86b77b0_59707010',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_3656795776063caa86b7a94_23450090',
  ),
  'page_content' => 
  array (
    0 => 'Block_3979912836063caa86b7f19_47793146',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_9510180596063caa86b8513_25077239',
  ),
  'page_footer' => 
  array (
    0 => 'Block_6428178876063caa86b87b4_29024854',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main"> 

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2984652576063caa86b6f57_88993821', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15260574056063caa86b77b0_59707010', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9510180596063caa86b8513_25077239', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
