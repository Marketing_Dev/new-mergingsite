<?php
/* Smarty version 3.1.34-dev-7, created on 2021-03-31 09:04:39
  from '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_6063caa77c8b16_84832492',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '68a8dcb06c170322794cda821c6d17385a25a53d' => 
    array (
      0 => '/opt/lampp/htdocs/prestamerge/themes/child_classic_new/templates/index.tpl',
      1 => 1615769752,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063caa77c8b16_84832492 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5030300516063caa77c7187_44687734', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_6279321666063caa77c7592_63682786 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_15940877616063caa77c7e05_83386841 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_4026834316063caa77c7b37_02752496 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15940877616063caa77c7e05_83386841', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_5030300516063caa77c7187_44687734 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_5030300516063caa77c7187_44687734',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_6279321666063caa77c7592_63682786',
  ),
  'page_content' => 
  array (
    0 => 'Block_4026834316063caa77c7b37_02752496',
  ),
  'hook_home' => 
  array (
    0 => 'Block_15940877616063caa77c7e05_83386841',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6279321666063caa77c7592_63682786', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4026834316063caa77c7b37_02752496', 'page_content', $this->tplIndex);
?>


      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
