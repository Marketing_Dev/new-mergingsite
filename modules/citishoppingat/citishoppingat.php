<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class CitiShoppingAt extends Module implements WidgetInterface
{
    private $templateFile = '';
    public function __construct()
    {
        $this->name = 'citishoppingat';
        $this->author = 'LGGA';
        $this->version = '1.0.0';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Citi Shopping Location');
        $this->description = $this->l('A functionality which allows users to determine what store they are shopping');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);
        $this->templateFile = 'module:citishoppingat/views/templates/hook/CitiShoppingAt.tpl';

    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        if (!parent::install() or
            !$this->registerHook('displayNav1') or
            !$this->registerHook('displayNav')
        ) {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }
        return true;
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->active) {
            return;
        }
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

        return $this->display(__FILE__, 'views/templates/hooks/CitiShoppingAt.tpl');
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        return array(
            'location' => 'Matina - Balusong'
        );
    }
}
