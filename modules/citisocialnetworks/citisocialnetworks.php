<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;


class CitiSocialNetworks extends Module implements WidgetInterface
{
    private $templateFile = '';
    public function __construct()
    {
        $this->name = 'citisocialnetworks';
        $this->author = 'LGGA';
        $this->version = '1.0.0';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Citi Social Network Links');
        $this->description = $this->l('A feature which shows the CitiHardware\'s social network links ');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);
        $this->templateFile = 'module:citishoppingat/views/templates/hook/CitiSocialNetworks.tpl';

    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        if (!parent::install() or
            !$this->registerHook('displayFooter') or
            !$this->registerHook('header')
        ) {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }
        return true;
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->active) {
            return;
        }
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

        return $this->display(__FILE__, 'views/templates/hooks/CitiSocialNetworks.tpl');
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {

    }

    public function hookHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/font-awesome.min.css');
    }

}