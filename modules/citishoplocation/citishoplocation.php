<?php

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

if (!defined('_PS_VERSION_'))
    exit;

class CitiShopLocation extends Module implements WidgetInterface
{
    private $templateFile = '';

    public function __construct()
    {
        $this->name = "citishoplocation";
        $this->tab = "front_office_features";
        $this->version = "1.0.0";
        $this->author = "LGGA";
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('CitiShopLocation');
        $this->description = $this->l('This module shows a modal that allows the user to pick the store location');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('MYMODULE_NAME')) {
            $this->warning = $this->l('No name provided.');
        }
        $this->templateFile = "module:citishoplocation/views/templates/hook/hook.tpl";
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        if (
            !parent::install() or
            !$this->registerHook('displayNav1') or
            !$this->registerHook('header')
        )
            return false;
        return true;
    }

    public function uninstall()
    {
        $cookie = new Cookie('customer_cookie');
        $cookie->__unset('customer');
        $branch = new Cookie("branch_cookie");
        $branch->__unset('branch_selected');
        if (!parent::uninstall())
            return false;
        return true;
    }

    public function hookDisplayNav1($params)
    {
        $address = $this->context->shop->getAddress();
        $branch = new Cookie("branch_cookie");
        if (Tools::isSubmit('btnProceed')) {
            $url = Tools::getValue('branchSelected');
            $branch->__set('branch_selected', $url);
            $branch->write();
            header("Location: $url");
        } elseif ($branch->__isset('branch_selected')) {
            $url = $branch->__get('branch_selected');
            $shopmain = _PS_BASE_URL_ . '/';
            $curr_url = _PS_BASE_URL_ . __PS_BASE_URI__;
            if ($curr_url != $url) {
                header("Location: $url");
            }
        }
        $c = new Cookie('customer_cookie');
        $c->__unset('customer');

        $current_shop = $this->context->shop->name;
        $davao_b = array('balusong');
        $iloilo_b = array('iloilo');
        $branches = array();
        $bs = Db::getInstance()->executeS(
            'SELECT ps.`name`, psu.`virtual_uri`,
                CONCAT(psu.`domain`, psu.`physical_uri`, psu.`virtual_uri`) as url
                FROM ' . _DB_PREFIX_ . 'shop ps INNER JOIN ' . _DB_PREFIX_ . 'shop_url psu ON ps.`id_shop` = psu.`id_shop` WHERE ps.`id_shop_group` = 2 AND ps.`active` = 1 AND ps.`id_shop` NOT IN (1)'
        );

        foreach ($bs as $i => $branch) {
            if (in_array(strtolower($branch["name"]), $davao_b)) {
                $branches[$i]["name"] = "Davao - " . $branch["name"];
            } else if (in_array(strtolower($branch["name"]), $iloilo_b)) {
                $branches[$i]["name"] = "Iloilo - " . "Mandurriao";
            } else {
                $branches[$i]["name"] = $branch["name"];
            }
            $branches[$i]["url"] = $branch["url"];
        }

        if (in_array(strtolower($current_shop), $davao_b)) {
            $current_shop = "Davao - " . $current_shop;
        } else if (in_array(strtolower($current_shop), $iloilo_b)) {
            $current_shop = "Iloilo - Mandurriao";
        }

        $this->context->smarty->assign(array(
            'uri' => __FILE__,
            'branches' => $branches,
            'shop' => $current_shop,
            'contact_info' => [
                'address' => $address->address1,
                'city' => $address->city,
                'tel' => Configuration::get('PS_SHOP_PHONE'),
                'fax' => Configuration::get('PS_SHOP_FAX')
            ],
        ));

        return $this->display(__FILE__, 'views/templates/hook/hook.tpl');
    }

    public function hookHeader()
    {
        // '_pathcontroller' => $this->_path."controllers/front/selection.php",
        Media::addJsDef(array(
            '_pathcontroller' => $this->context->link->getModuleLink('citishoplocation', 'selection'),
        ));

        $this->context->controller->addCss(array(
            $this->_path . "/views/css/citishoplocation.css"
        ));
        $this->context->controller->addJs(array(
            $this->_path . "/views/js/citishoplocation.js"
        ));
    }


    public function renderWidget($hookName, array $configuration)
    {
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        return $this->fetch("module:citibranches/views/templates/hook/mobile.tpl");
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        $address = $this->context->shop->getAddress();

        $current_shop = $this->context->shop->name;
        $davao_b = array('balusong');
        $iloilo_b = array('iloilo');
        $branches = array();
        $bs = Db::getInstance()->executeS(
            'SELECT ps.`name`, psu.`virtual_uri`,
                CONCAT(psu.`domain`, psu.`physical_uri`, psu.`virtual_uri`) as url
                FROM ' . _DB_PREFIX_ . 'shop ps INNER JOIN ' . _DB_PREFIX_ . 'shop_url psu ON ps.`id_shop` = psu.`id_shop` WHERE ps.`id_shop_group` = 2 AND ps.`active` = 1 AND ps.`id_shop` NOT IN (1)'
        );

        foreach ($bs as $i => $branch) {
            if (in_array(strtolower($branch["name"]), $davao_b)) {
                $branches[$i]["name"] = "Davao - " . $branch["name"];
            } else if (in_array(strtolower($branch["name"]), $iloilo_b)) {
                $branches[$i]["name"] = "Iloilo - " . "Mandurriao";
            } else {
                $branches[$i]["name"] = $branch["name"];
            }
            $branches[$i]["url"] = $branch["url"];
        }

        if (in_array(strtolower($current_shop), $davao_b)) {
            $current_shop = "Davao - " . $current_shop;
        } else if (in_array(strtolower($current_shop), $iloilo_b)) {
            $current_shop = "Iloilo - Mandurriao";
        }

        return array(
            'uri' => __FILE__,
            'branches' => $branches,
            'shop' => $current_shop,
            'contact_info' => [
                'address' => $address->address1,
                'city' => $address->city,
                'tel' => Configuration::get('PS_SHOP_PHONE'),
                'fax' => Configuration::get('PS_SHOP_FAX')
            ],
        );
    }
}