const template = `<div class="faceted-overlay">
 <div class="overlay__inner">
 <div class="overlay__content"><span class="spinner"></span></div>
 </div>
 </div>`;

$('body').prepend(template);


$.ajax({
    url : _pathcontroller,
    cache : false,
    type : 'POST',
    data : {
        ajax: true,
        action: "fetchTemplate",
    },
    success : function(result) {
        if(!(result.includes("none"))){
            $('.faceted-overlay').remove();
            $('body').append(result);
            $('#citiSelection').modal({
                show: true,
                backdrop: 'static'
            });
            getBranchDetails();
        }
    },
});

$( document ).ajaxComplete(function( event,request, settings ) {
    $(document).ready(function(){ $('.faceted-overlay').remove();  });
});

$(document).ready(function(){setBranchHeaderCookie();});

function getBranchDetails(){
    $id_shop = $('#citibranchSelection').val();
    $('#citibranchSelection').on('change', function(){
        $id_shop = $(this).find(':selected').data('id');
        if($id_shop != ''){
            getBranchInfo($id_shop);
        }else{
            $('.branchInfo').html('');
        }
    });

}

function getBranchInfo(id_shop){
    $.ajax({
        url : _pathcontroller,
        cache : false,
        type : 'POST',
        data : {
            ajax: true,
            variable_1 : id_shop,
            action: "getShopInfo",
        },
        success : function(result){
            $('.branchInfo').html(result);
        }
    })
}

function setBranchHeaderCookie(){
    $('.drd-item').on('click touchend', function(e){
        var hrefUrl = $(this).attr("href");
        e.preventDefault();
        $.ajax({
            url : _pathcontroller,
            cache : false,
            type : 'POST',
            data : {
                ajax: true,
                variable_1 : hrefUrl,
                action: "setBranchCookie",
            },
            beforeSend : function(){
                $('body').prepend(template);
            },
            success : function(result){
                location.replace(hrefUrl);
            },
            error: function(jqXHR,error, errorThrown){

            }
        })
    });
}




