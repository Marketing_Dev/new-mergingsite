<?php

class CitiShopLocationSelectionModuleFrontContoller extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function displayAjaxFetchTemplate()
    {
        $branch = new Cookie("branch_cookie");
        if(!$branch->__isset('branch_selected')){

            $davao_b = array('balusong');
            $iloilo_b = array('iloilo');
            $branches = array();
            $bs = Db:: stance()->executeS(
                'SELECT ps.`name`, ps.`id_shop` , psu.`virtual_uri`,
                    CONCAT(psu.`domain`, psu.`physical_uri`, psu.`virtual_uri`) as url
                    FROM '._DB_PREFIX_.'shop ps INNER JOIN '._DB_PREFIX_.'shop_url psu ON ps.`id_shop` = psu.`id_shop` WHERE ps.`id_shop_group` = 2 AND ps.`active` = 1 AND ps.`id_shop` NOT IN (1)'
            );

            foreach($bs as $i=>$branch){
                if(in_array(strtolower($branch["name"]), $davao_b)){
                    $branches[$i]["name"] = "Davao - " . $branch["name"];
                }else if(in_array(strtolower($branch["name"]), $iloilo_b)){
                    $branches[$i]["name"] = "Iloilo - Mandurriao";
                }else{
                    $branches[$i]["name"] = $branch["name"];
                }
                $branches[$i]["url"] = $branch["url"];
                $branches[$i]["id_shop"] = $branch["id_shop"];
            }

            $data = array(
                'uri' => __FILE__ ,
                'branches' =>  $branches,
                'shop' => $this->context->shop->name,
            );

            $this->context->smarty->assign(array(
                    'content_data' => $data)
            );
            die($this->context->smarty->fetch(_PS_MODULE_DIR_.'citibranches/views/templates/front/modal.tpl'));
        }
        echo 'none';
    }

    public function displayAjaxGetShopInfo(){
        $id_shop = Tools::getValue('variable_1');

        $address = new Address();
        // $address->company = Configuration::get('PS_SHOP_NAME', null, null, $id_shop);
        $address->address1 =  Configuration::get('PS_SHOP_ADDR1', null, null, $id_shop);
        $address->city =  Configuration::get('PS_SHOP_CITY', null, null, $id_shop);
        $address->phone = "Call Us: ". Configuration::get('PS_SHOP_PHONE', null, null, $id_shop);
        $fax = Configuration::get('PS_SHOP_FAX');

        $shop_name = Configuration::get('PS_SHOP_NAME', null, null, $id_shop);

        $address_output = "<div class='row mt-2'> <div class='col-md-12'><h4>$shop_name</h4>";
        $address_output .= AddressFormat::generateAddress($address, array(), '<br />', ' ') . "
        <br/>Fax: $fax
        </div></div>";

        echo $address_output;
    }

    public function displayAjaxSetBranchCookie(){
        $branch = new Cookie("branch_cookie");
        $url = Tools::getValue('variable_1');
        $branch->__set('branch_selected', $url);
        $branch->write();
        echo 'overwritten';
    }
}