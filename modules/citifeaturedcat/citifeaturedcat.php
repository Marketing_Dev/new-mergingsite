<?php

if(!defined('_PS_VERSION_'))
    exit;

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;


class CitiFeaturedCat extends Module implements WidgetInterface
{
   public function __construct()
   {
       $this->name = 'citifeaturedcat';
       $this->author = 'LGGA';
       $this->version = '1.0.0';
       $this->bootstrap = true;

       parent::__construct();

       $this->displayName = $this->trans('Home Featured Category', [], 'Modules.CitiFeaturedProd.Admin');
       $this->description = $this->trans(
           'Adds a featured category section in home page',
           [],
           'Modules.CitiFeaturedCat.Admin'
       );
       $this->ps_versions_compliancy = [
           'min' => '1.7.2.0',
           'max' => _PS_VERSION_
       ];

       $this->templateFile = 'module:citifeaturedcat/views/templates/hooks/citifeaturedcat.tpl';

   }

    public function install()
    {
        return parent::install()
            && $this->registerHook([
                'displayHome', // For Classic-inspired themes
                'displayHeader',
            ]);
    }

    public function hookDisplayHome()
    {
        return $this->display(__FILE__, 'views/templates/hooks/citifeaturedcat.tpl');
    }
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/citifeaturedcat.css', ['media' => 'all', 'priority' => 150]);
        $this->context->controller->addJS($this->_path.'views/js/citifeaturedcat.js', ['position' => 'bottom', 'priority' => 150]);
    }

    public function renderWidget($hookName, array $configuration)
    {
        if (!$this->active) {
            return;
        }
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));

    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        // TODO: Implement getWidgetVariables() method.
        return [
            'images' => [
                'image1' => $this->context->link->getMediaLink(_MODULE_DIR_.'citifeaturedcat/views/images/dummy.jpg'),
                'image2' => $this->context->link->getMediaLink(_MODULE_DIR_.'citifeaturedcat/views/images/dummy.jpg'),
                'image3' => $this->context->link->getMediaLink(_MODULE_DIR_.'citifeaturedcat/views/images/dummy.jpg'),
            ],
        ];
    }
}
