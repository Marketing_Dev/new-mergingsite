<?php

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

if (!defined('_PS_VERSION_')) {
    exit;
}

class CitiContactUs extends Module implements WidgetInterface
{

    public function __construct()
    {
        $this->name = 'citicontactus';
        $this->author = 'LGGA';
        $this->version = '1.0.0';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->getTranslator()->trans('Customer "Join us" link');
        $this->description = $this->getTranslator()->trans('Adds a block that displays the join us link on the nav');
        $this->ps_versions_compliancy = ['min' => '1.7.1.0', 'max' => _PS_VERSION_];

        $this->templateFile = 'module:citicontactus/views/templates/hooks/citicontactus.tpl';
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook([
                'displayNav2', // For Classic-inspired themes
            ]);
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        return $this->fetch('module:' . $this->name . '/' . 'citicontactus.tpl');
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
    }
}
