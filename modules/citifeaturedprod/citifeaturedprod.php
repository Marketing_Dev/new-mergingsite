<?php

if(!defined('_PS_VERSION_'))
    exit;

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class CitiFeaturedProd extends Module implements WidgetInterface
{
    public function __construct()
    {
        $this->name = 'citifeaturedprod';
        $this->author = 'LGGA';
        $this->version = '1.0.0';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->trans('Home Featured Products', [], 'Modules.CitiFeaturedProd.Admin');
        $this->description = $this->trans(
            'Adds a featured products section in home page',
            [],
            'Modules.CitiFeaturedProd.Admin'
        );
        $this->ps_versions_compliancy = [
            'min' => '1.7.2.0',
            'max' => _PS_VERSION_
        ];

        $this->templateFile = 'module:citifeaturedprod/views/templates/hooks/citifeaturedprod.tpl';

    }

    public function install()
    {
        return parent::install()
            && $this->registerHook([
                'displayHome', // For Classic-inspired themes
                'displayHeader',
            ]);
    }

    public function hookDisplayHome()
    {
        return $this->display(__FILE__, 'views/templates/hooks/citifeaturedprod.tpl');
    }
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/citifeaturedprod.css', ['media' => 'all', 'priority' => 150]);
        $this->context->controller->addJS($this->_path.'views/js/citifeaturedprod.js', ['position' => 'bottom', 'priority' => 150]);
    }

    public function renderWidget($hookName, array $configuration)
    {
        // TODO: Implement renderWidget() method.
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        // TODO: Implement getWidgetVariables() method.
    }
}