<?php

class CitiBannerImages extends Module
{
    public function __construct()
    {
        $this->name = "citibannerimages";
        $this->author = "LGGA";
        $this->version = '1.0.0';
        $this->need_instance = 1;
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Citi Banner Images');
        $this->description = $this->l('Put Banner Images on the pages');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall module?');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }


    public function install()
    {
        return parent::install()
            && $this->registerHook([
                'displayBannerTop',
            ]);
    }

    public function hookDisplayBannerTop()
    {
        return $this->display(__FILE__, 'views/templates/hooks/citibannerimages.tpl');
    }
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/citibannerimages.css', ['media' => 'all', 'priority' => 150]);
    }

    public function renderWidget($hookName, array $configuration)
    {
        // TODO: Implement renderWidget() method.
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
        // TODO: Implement getWidgetVariables() method.
    }

}