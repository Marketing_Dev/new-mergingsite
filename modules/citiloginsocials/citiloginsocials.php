<?php

class CitiLoginSocials extends Module
{
    public function __construct()
    {
        $this->name = "citiloginsocials";
        $this->author = "LGGA";
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->need_instance = 1;
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Citi Login Social Buttons');
        $this->description = $this->l('Customer can login using their Google and Facebook accounts');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall module?');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }


    public function install()
    {
        Configuration::updateValue('SOCIALLOGIN_LIVE_MODE', false);
        return parent::install() && $this->registerHook('displayCustomerLoginFormAfter') && $this->registerHook('header');
    }

    public function uninstall()
    {
        Configuration::deleteByName('SOCIALLOGIN_LIVE_MODE');
        return parent::uninstall();
    }


    public function hookDisplayCustomerLoginFormAfter()
    {
        if (!$this->active)
            return;
        return $this->fetch('module:citiloginsocials/views/templates/front/CitiLoginSocials.tpl');
    }

    public function hookHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/font-awesome.min.css');
    }

}